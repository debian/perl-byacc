#!/usr/bin/make -f
# -*- makefile -*-
# debian/rules for perl-byacc

export SHELL=/bin/bash

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

export CFLAGS += -ffile-prefix-map=$(CURDIR)=.

package	:= $(firstword $(shell dh_listpackages))
destdir	:= $(CURDIR)/debian/$(package)
doc	:= $(destdir)/usr/share/doc/$(package)
examples	:= $(doc)/examples/perl5

version	:= $(shell dpkg-parsechangelog | \
			sed -ne 's/^Version: *\([0-9]\+:\)*//p')

tag:
	cvs tag -c -F $(subst .,_,debian_version_$(version))
ifeq ($(findstring -,$(version)),)
	cvs tag -c -F $(subst .,_,upstream_version_$(version))
endif

build: build-arch build-indep
build-arch: build-stamp
build-indep: build-stamp
build-stamp:
	dh_testdir
	$(MAKE) 
	touch $@

clean:
	dh_testdir
	rm -f *-stamp
	-$(MAKE) clobber
	-$(RM) pbyacc.man
	dh_clean

binary-indep: build

binary-arch: build
	dh_testdir
	dh_clean
	dh_installdirs
	$(MAKE) install DESTDIR=$(destdir)
	dh_installdocs ACKNOWLEDGEMENTS NEW_FEATURES NO_WARRANTY
	install -m 0644 perl5-patch/README $(doc)/README.Perl5
	dh_installexamples -Xerror.tab.h test/*
	install -D -m 0644 perl5-patch/Makefile $(examples)/Makefile
	install -m 0644 perl5-patch/{Makefile,*.y,*.pl,*.pm,thought} $(examples)
	dh_installman
	dh_installchangelogs
	dh_strip
	dh_compress
	dh_fixperms
	dh_perl
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch

.PHONY: binary binary-indep binary-arch clean build
